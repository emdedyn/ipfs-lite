package threads.server.ipfs;

public interface ReaderProgress extends Progress {
    long getSize();
}
