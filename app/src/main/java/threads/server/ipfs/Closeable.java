package threads.server.ipfs;


public interface Closeable {
    boolean isClosed();
}
